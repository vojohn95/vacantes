<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Log
 * @package App\Models
 * @version August 22, 2019, 3:04 pm UTC
 *
 * @property \App\Models\User idUser
 * @property \Illuminate\Database\Eloquent\Collection
 * @property integer id_user
 * @property string mensaje_sis
 * @property string url
 * @property string metodo
 * @property string tipo
 */
class Log extends Model
{

    public $table = 'log';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'id_user',
        'mensaje_sis',
        'url',
        'metodo',
        'tipo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_user' => 'integer',
        'mensaje_sis' => 'string',
        'url' => 'string',
        'metodo' => 'string',
        'tipo' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_user' => 'required',
        'mensaje_sis' => 'required',
        'url' => 'required',
        'metodo' => 'required',
        'tipo' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idUser()
    {
        return $this->belongsTo(\App\Models\User::class, 'id_user');
    }
}

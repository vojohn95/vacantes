<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Gerente
 * @package App\Models
 * @version August 22, 2019, 3:06 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection proyectos
 * @property \Illuminate\Database\Eloquent\Collection
 * @property string nombre
 * @property string email
 * @property string telefono
 * @property string empresa
 * @property string estado
 */
class Gerente extends Model
{

    public $table = 'gerentes';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'nombre',
        'email',
        'telefono',
        'empresa',
        'estado'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string',
        'email' => 'string',
        'telefono' => 'string',
        'empresa' => 'string',
        'estado' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'email' => 'required',
        'telefono' => 'required',
        'empresa' => 'required',
        'estado' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function proyectos()
    {
        return $this->hasMany(\App\Models\Proyecto::class);
    }
}

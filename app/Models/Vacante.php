<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Vacante
 * @package App\Models
 * @version August 22, 2019, 2:59 pm UTC
 *
 * @property \App\Models\Proyecto idProyecto
 * @property \App\Models\Puesto idPuesto
 * @property \App\Models\User idUser
 * @property \Illuminate\Database\Eloquent\Collection postulantes
 * @property \Illuminate\Database\Eloquent\Collection
 * @property integer id_proyecto
 * @property integer id_puesto
 * @property integer id_user
 * @property string horario
 * @property float salario
 * @property string fecha
 * @property string tel
 * @property string email
 * @property string estado
 */
class Vacante extends Model
{

    public $table = 'vacantes';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public $fillable = [
        'id_proyecto',
        'id_puesto',
        'id_user',
        'horario',
        'salario',
        'fecha',
        'tel',
        'email',
        'estado'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_proyecto' => 'integer',
        'id_puesto' => 'integer',
        'id_user' => 'integer',
        'horario' => 'string',
        'salario' => 'float',
        'fecha' => 'date',
        'tel' => 'string',
        'email' => 'string',
        'estado' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_proyecto' => 'required',
        'id_puesto' => 'required',
        'id_user' => 'required',
        'horario' => 'required',
        'salario' => 'required',
        'fecha' => 'required',
        'tel' => 'required',
        'email' => 'required',
        'estado' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idProyecto()
    {
        return $this->belongsTo(\App\Models\Proyecto::class, 'id_proyecto');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idPuesto()
    {
        return $this->belongsTo(\App\Models\Puesto::class, 'id_puesto');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idUser()
    {
        return $this->belongsTo(\App\Models\User::class, 'id_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function postulantes()
    {
        return $this->hasMany(\App\Models\Postulante::class);
    }
}

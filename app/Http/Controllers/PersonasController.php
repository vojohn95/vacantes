<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PersonasController extends Controller
{
    public function index(){
        $pais = DB::table('vacantes')
            ->join('proyectos', 'proyectos.id', '=', 'vacantes.id_proyecto')
            ->join('puestos', 'puestos.id', '=', 'vacantes.id_puesto')
            ->select('proyectos.pais')
            ->distinct('proyectos.pais')
            ->get();
        $proyectos = DB::table('vacantes')
            ->join('proyectos', 'proyectos.id', '=', 'vacantes.id_proyecto')
            ->join('puestos', 'puestos.id', '=', 'vacantes.id_puesto')
            ->select('proyectos.id', 'proyectos.nombre', 'proyectos.no_est')
            ->distinct('proyectos.id')
            ->get();
        $puestos = DB::table('vacantes')
            ->join('proyectos', 'proyectos.id', '=', 'vacantes.id_proyecto')
            ->join('puestos', 'puestos.id', '=', 'vacantes.id_puesto')
            ->select('puestos.id', 'puestos.puesto')
            ->distinct('puestos.puesto')
            ->get();
        $marcas = DB::table('vacantes')
            ->join('proyectos', 'proyectos.id', '=', 'vacantes.id_proyecto')
            ->join('puestos', 'puestos.id', '=', 'vacantes.id_puesto')
            ->join('marcas','marcas.id','=','proyectos.id_marca')
            ->select('marcas.id', 'marcas.marca')
            ->distinct('marcas.id')
            ->get();
        return view('Personas.home')
            ->with('pais', $pais)
            ->with('proyectos',$proyectos)
            ->with('estados',$proyectos)
            ->with('puestos',$puestos)
            ->with('marcas',$marcas);
    }

    //Peticion de Puestos
    public function AjaxRequestPuestos(Request $request)
    {
        $res = "";
        $input = $request->all();
        $puestosconsultados = DB::table('vacantes')
            ->join('proyectos', 'proyectos.id', '=', 'vacantes.id_proyecto')
            ->join('puestos', 'puestos.id', '=', 'vacantes.id_puesto')
            ->where('vacantes.id_puesto', '=', $input['valor'])
            ->select('vacantes.id', 'proyectos.nombre', 'proyectos.direccion', 'puestos.puesto')
            ->get();
        $header = '<br><div class="table-responsive">
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead>
        <tr>
            <th>Estacionamiento</th>
            <th>Direccion</th>
            <th>Puesto</th>
            <th>Acción</th>

        </tr>
        </thead>
        <tbody>';
        $res .= $header;
        foreach ($puestosconsultados as $item) {
            $body = '<tr>
            <td>' . $item->nombre . '</td>
            <td>' . $item->direccion . '</td>
            <td>' . $item->puesto . '</td>
            <td>
                <a href="' . route('rutas', $item->id) . '" class=\'btn btn-default btn-xs white-text\'><i class="glyphicon glyphicon-edit"></i>Detalles</a>
            </td>
            </tr>
            ';
            $res .= $body;
        }
        $end = '</tbody>
    </table>';
        $res .= $end;
        return response()->json($res, 200);
    }

    //Peticion de Estacionamientos
    public function AjaxRequestEstacionamiento(Request $request)
    {
        $input = $request->all();
        $proyectos = DB::table('vacantes')
            ->join('proyectos', 'proyectos.id', '=', 'vacantes.id_proyecto')
            ->join('puestos', 'puestos.id', '=', 'vacantes.id_puesto')
            ->select('vacantes.id', 'proyectos.nombre', 'proyectos.direccion', 'puestos.puesto')
            ->where('proyectos.id', '=', $input['valor'])
            ->get();
        $res = "";
        $header = '<br><div class="table-responsive">
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead>
        <tr>
            <th>Estacionamiento</th>
            <th>Direccion</th>
            <th>Puesto</th>
            <th>Acción</th>

        </tr>
        </thead>
        <tbody>';
        $res .= $header;
        foreach ($proyectos as $item) {
            $body = '<tr>
            <td>' . $item->nombre . '</td>
            <td>' . $item->direccion . '</td>
            <td>' . $item->puesto . '</td>
            <td>
                <a href="' . route('rutas', $item->id) . '" class=\'btn btn-default btn-xs white-text\'><i class="glyphicon glyphicon-edit"></i>Detalles</a>
            </td>
            </tr>
            ';
            $res .= $body;
        }
        $end = '</tbody>
    </table>';
        $res .= $end;
        return response()->json($res, 200);
    }

    //Peticion de Marcas
    public function AjaxRequestMarcas(Request $request)
    {
        $input = $request->all();
        $marcas = DB::table('vacantes')
            ->join('proyectos', 'proyectos.id', '=', 'vacantes.id_proyecto')
            ->join('puestos', 'puestos.id', '=', 'vacantes.id_puesto')
            ->join('marcas','marcas.id','=','proyectos.id_marca')
            ->select('vacantes.id', 'proyectos.nombre', 'proyectos.direccion', 'puestos.puesto')
            ->where('marcas.id', '=', $input['valor'])
            ->get();
        $res = "";
        $header = '<br><div class="table-responsive">
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead>
        <tr>
            <th>Estacionamiento</th>
            <th>Direccion</th>
            <th>Puesto</th>
            <th>Acción</th>

        </tr>
        </thead>
        <tbody>';
        $res .= $header;
        foreach ($marcas as $item) {
            $body = '<tr>
            <td>' . $item->nombre . '</td>
            <td>' . $item->direccion . '</td>
            <td>' . $item->puesto . '</td>
            <td>
                <a href="' . route('rutas', $item->id) . '" class=\'btn btn-default btn-xs white-text\'><i class="glyphicon glyphicon-edit"></i>Detalles</a>
            </td>
            </tr>
            ';
            $res .= $body;
        }
        $end = '</tbody>
    </table>';
        $res .= $end;
        return response()->json($res, 200);
    }

    //Peticion de Pais
    public function AjaxRequestPais(Request $request)
    {
        $input = $request->all();
        $pais = DB::table('vacantes')
            ->join('proyectos', 'proyectos.id', '=', 'vacantes.id_proyecto')
            ->join('puestos', 'puestos.id', '=', 'vacantes.id_puesto')
            ->where('proyectos.pais', '=', $input['pais'])
            ->select('proyectos.edo_prov')
            ->distinct('proyectos.edo_prov')
            ->get();
        $res = ' <select class="mdb-select2 md-form colorful-select dropdown-default" searchable="Buscar..."
                                name="estacionamiento" id="estado_busca" onchange="estado()">
                            <option value="" selected disabled>Seleccione un Estado</option>';
        foreach ($pais as $item) {
            $res .= '<option value="' . $item->edo_prov . '">' . $item->edo_prov . '</option>';
        }
        $res .= '</select>';
        return response()->json($res, 200);
    }

    //Peticion de Estado
    public function AjaxRequestEstado(Request $request)
    {
        $input = $request->all();
        $estado = DB::table('vacantes')
            ->join('proyectos', 'proyectos.id', '=', 'vacantes.id_proyecto')
            ->join('puestos', 'puestos.id', '=', 'vacantes.id_puesto')
            ->where('proyectos.pais', '=', $input['pais'])
            ->where('proyectos.edo_prov', '=', $input['estado'])
            ->select('proyectos.mun_cd')
            ->distinct('proyectos.mun_cd')
            ->get();
        $res = ' <select class="mdb-select3 md-form colorful-select dropdown-default" searchable="Buscar..."
                                name="estacionamiento" id="munic" onchange="mun()">
                            <option value="" selected disabled>Seleccione un municipio o delegacion</option>';
        foreach ($estado as $item) {
            $res .= '<option value="' . $item->mun_cd . '">' . $item->mun_cd . '</option>';
        }
        $res .= '</select>';
        return response()->json($res, 200);
    }

    //Peticion de Municipio
    public function AjaxRequestMunicipio(Request $request)
    {
        $input = $request->all();
        $municipio = DB::table('vacantes')
            ->join('proyectos', 'proyectos.id', '=', 'vacantes.id_proyecto')
            ->join('puestos', 'puestos.id', '=', 'vacantes.id_puesto')
            ->where('proyectos.pais', '=', $input['pais'])
            ->where('proyectos.edo_prov', '=', $input['estado'])
            ->where('proyectos.mun_cd', '=', $input['municipio'])
            ->select('vacantes.id', 'proyectos.nombre', 'proyectos.direccion', 'puestos.puesto')
            ->get();
        $res = "";
        $header = '<br><div class="table-responsive">
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead>
        <tr>
            <th>Estacionamiento</th>
            <th>Direccion</th>
            <th>Puesto</th>
            <th>Acción</th>

        </tr>
        </thead>
        <tbody>';
        $res .= $header;
        foreach ($municipio as $item) {
            $body = '<tr>
            <td>' . $item->nombre . '</td>
            <td>' . $item->direccion . '</td>
            <td>' . $item->puesto . '</td>
            <td>
                <a href="' . route('rutas', $item->id) . '" class=\'btn btn-default btn-xs white-text\'><i class="glyphicon glyphicon-edit"></i>Detalles</a>
            </td>
            </tr>
            ';
            $res .= $body;
        }
        $end = '</tbody>
    </table>';
        $res .= $end;
        return response()->json($res, 200);
    }

    public function rutas($id)
    {
        $proyectos = DB::table('vacantes')
            ->join('proyectos', 'proyectos.id', '=', 'vacantes.id_proyecto')
            ->join('puestos', 'puestos.id', '=', 'vacantes.id_puesto')
            ->where('vacantes.id', '=', $id)
            ->select('vacantes.id as unico', 'proyectos.*', 'puestos.*', 'vacantes.*')
            ->get();
        return view('Personas.ruta')
            ->with('proyectos', $proyectos);
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateVacanteAPIRequest;
use App\Http\Requests\API\UpdateVacanteAPIRequest;
use App\Models\Vacante;
use App\Repositories\VacanteRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class VacanteController
 * @package App\Http\Controllers\API
 */
class VacanteAPIController extends AppBaseController
{
    /** @var  VacanteRepository */
    private $vacanteRepository;

    public function __construct(VacanteRepository $vacanteRepo)
    {
        $this->vacanteRepository = $vacanteRepo;
    }

    /**
     * Display a listing of the Vacante.
     * GET|HEAD /vacantes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $vacantes = $this->vacanteRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($vacantes->toArray(), 'Vacantes retrieved successfully');
    }

    /**
     * Store a newly created Vacante in storage.
     * POST /vacantes
     *
     * @param CreateVacanteAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateVacanteAPIRequest $request)
    {
        $input = $request->all();

        $vacante = $this->vacanteRepository->create($input);

        return $this->sendResponse($vacante->toArray(), 'Vacante saved successfully');
    }

    /**
     * Display the specified Vacante.
     * GET|HEAD /vacantes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Vacante $vacante */
        $vacante = $this->vacanteRepository->find($id);

        if (empty($vacante)) {
            return $this->sendError('Vacante not found');
        }

        return $this->sendResponse($vacante->toArray(), 'Vacante retrieved successfully');
    }

    /**
     * Update the specified Vacante in storage.
     * PUT/PATCH /vacantes/{id}
     *
     * @param int $id
     * @param UpdateVacanteAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVacanteAPIRequest $request)
    {
        $input = $request->all();

        /** @var Vacante $vacante */
        $vacante = $this->vacanteRepository->find($id);

        if (empty($vacante)) {
            return $this->sendError('Vacante not found');
        }

        $vacante = $this->vacanteRepository->update($input, $id);

        return $this->sendResponse($vacante->toArray(), 'Vacante updated successfully');
    }

    /**
     * Remove the specified Vacante from storage.
     * DELETE /vacantes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Vacante $vacante */
        $vacante = $this->vacanteRepository->find($id);

        if (empty($vacante)) {
            return $this->sendError('Vacante not found');
        }

        $vacante->delete();

        return $this->sendResponse($id, 'Vacante deleted successfully');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gerentes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->string('email')->unique();
            $table->string('telefono');
            $table->string('empresa');
            $table->string('estado')->default('ACTIVO');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('marcas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('marca');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('proyectos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_gerente');
            $table->foreign('id_gerente')
                ->references('id')
                ->on('gerentes')
                ->onDelete('cascade');
            $table->unsignedBigInteger('id_marca');
            $table->foreign('id_marca')
                ->references('id')
                ->on('marcas')
                ->onDelete('cascade');
            $table->string('nombre');
            $table->string('direccion');
            $table->string('pais');
            $table->string('edo_prov');
            $table->string('mun_cd');
            $table->string('colonia');
            $table->string('calles');
            $table->string('numeroExt');
            $table->string('cp');
            $table->string('latitud');
            $table->string('longitud');
            $table->boolean('status')->default(true);
            $table->integer('num_vac')->default(0);
            $table->longText('foto');
            $table->boolean('escuela')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('puestos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('puesto');
            $table->string('area');
            $table->text('req');
            $table->text('des');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('vacantes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_proyecto');
            $table->foreign('id_proyecto')
                ->references('id')
                ->on('proyectos')
                ->onDelete('cascade');
            $table->unsignedBigInteger('id_puesto');
            $table->foreign('id_puesto')
                ->references('id')
                ->on('puestos')
                ->onDelete('cascade');
            $table->unsignedBigInteger('id_user');
            $table->foreign('id_user')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->string('horario');
            $table->decimal('salario');
            $table->date('fecha');
            $table->string('tel');
            $table->string('email');
            $table->string('estado');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('postulantes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_vacante');
            $table->foreign('id_vacante')
                ->references('id')
                ->on('vacantes')
                ->onDelete('cascade');
            $table->string('nombre');
            $table->string('correo');
            $table->string('tel');
            $table->string('direccion');
            $table->string('pais');
            $table->string('edo_prov');
            $table->string('mun_cd');
            $table->string('colonia');
            $table->string('calles');
            $table->string('numeroExt');
            $table->string('cp');
            $table->string('estado');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('Log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_user');
            $table->foreign('id_user')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->string('mensaje_sis');
            $table->string('url');
            $table->string('metodo');
            $table->string('tipo');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('permisos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_user');
            $table->foreign('id_user')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->unsignedBigInteger('id_permiso');
            $table->foreign('id_permiso')
                ->references('id')
                ->on('permisos')
                ->onDelete('cascade');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gerentes');
        Schema::dropIfExists('proyectos');
        Schema::dropIfExists('puestos');
        Schema::dropIfExists('users');
        Schema::dropIfExists('vacantes');
        Schema::dropIfExists('postulantes');
        Schema::dropIfExists('Log');
        Schema::dropIfExists('permisos');
        Schema::dropIfExists('roles');
    }
}

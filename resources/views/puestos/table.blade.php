<div class="table-responsive">
    <div class="form-group pull-right">
        <input type="text" class="search form-control" placeholder="¿Que estas buscando?">
    </div>
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead >
        <tr>
            <th>Puesto</th>
            <th>Area</th>
            <th>Requerimientos</th>
            <th>Descripción</th>
            <th>Acción</th>

        </tr>
        <tr class="warning no-result">
            <td colspan="12" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i> No se encontro
                registro con la información ingresada
            </td>
        </tr>
        </thead>

        <tbody>
        @foreach($puestos as $puesto)
            <tr>
                <td>{!! $puesto->puesto !!}</td>
                <td>{!! $puesto->area !!}</td>
                <td>{!! $puesto->req !!}</td>
                <td>{!! $puesto->des !!}</td>
                {!! Form::open(['route' => ['puestos.destroy', $puesto->id], 'method' => 'delete']) !!}
                <td>
                    <a href="{!! route('puestos.edit', [$puesto->id]) !!}" class='btn btn-default btn-xs white-text'><i
                            class="glyphicon glyphicon-edit"></i>visualizar</a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"><span>Eliminar</span></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

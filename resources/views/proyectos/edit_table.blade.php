<!-- Nombre Field -->
<div class="md-form">
    <i class="far fa-address-card prefix"></i>
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Direccion Field -->
<div class="md-form">
    <i class="fas fa-directions prefix"></i>
    {!! Form::label('direccion', 'Direccion:') !!}
    {!! Form::text('direccion', null, ['class' => 'form-control']) !!}
</div>

<!-- Pais Field -->
<div class="md-form">
    <i class="fas fa-city prefix"></i>
    {!! Form::label('pais', 'País:') !!}
    {!! Form::text('pais', null, ['class' => 'form-control']) !!}
</div>

<!-- Edo Prov Field -->
<div class="md-form">
    <i class="fas fa-map-marked-alt prefix"></i>
    {!! Form::label('edo_prov', 'Estado/Provincia:') !!}
    {!! Form::text('edo_prov', null, ['class' => 'form-control']) !!}
</div>

<!-- Mun Cd Field -->
<div class="md-form">
    <i class="fas fa-map-signs prefix"></i>
    {!! Form::label('mun_cd', 'Municipio:') !!}
    {!! Form::text('mun_cd', null, ['class' => 'form-control']) !!}
</div>

<!-- Colonia Field -->
<div class="md-form">
    <i class="fas fa-map-pin prefix"></i>
    {!! Form::label('colonia', 'Colonia:') !!}
    {!! Form::text('colonia', null, ['class' => 'form-control']) !!}
</div>


<!-- Calles Field -->
<div class="md-form">
    <i class="fas fa-road prefix"></i>
    {!! Form::label('calles', 'Calles:') !!}
    {!! Form::text('calles', null, ['class' => 'form-control']) !!}
</div>

<!-- Numeroext Field -->
<div class="md-form">
    <i class="fas fa-map-marker prefix"></i>
    {!! Form::label('numeroExt', 'Numeroext:') !!}
    {!! Form::text('numeroExt', null, ['class' => 'form-control']) !!}
</div>

<!-- Cp Field -->
<div class="md-form">
    <i class="fas fa-map-marker-alt prefix"></i>
    {!! Form::label('cp', 'Cp:') !!}
    {!! Form::text('cp', null, ['class' => 'form-control']) !!}
</div>

<!-- Latitud Field -->
<!--<div class="md-form">
    {!! Form::label('latitud', 'Latitud:') !!}
{!! Form::text('latitud', null, ['class' => 'form-control']) !!}
        </div>

        <div class="md-form">
{!! Form::label('longitud', 'Longitud:') !!}
{!! Form::text('longitud', null, ['class' => 'form-control']) !!}
        </div>-->

<!-- Status Field -->
<!--<div class="md-form">
    <i class="far fa-lightbulb prefix"></i>
    <div class="form-check">
        <input type="checkbox" class="form-check-input" id="materialChecked2" checked>
        <label class="form-check-label" for="materialChecked2">Material checked</label>
    </div>
</div>-->

<!-- Num Vac Field -->
<div class="md-form">
    <i class="fas fa-list-ol prefix"></i>
    {!! Form::label('num_vac', 'Num Vac:') !!}
    {!! Form::number('num_vac', null, ['class' => 'form-control']) !!}
</div>

<!-- Foto Field -->
<div class="md-form">
    <div class="file-field">
        <div class="btn btn-primary btn-sm float-left">
            <span>Examinar equipo</span>
            <input type="file" name="file" id="file"
                   accept="image/*">
        </div>
        <div class="file-path-wrapper">
            <input class="file-path validate"  type="text" placeholder="Cargar archivo">
        </div>
    </div>

</div>

<!-- Escuela Field -->
<!--<div class="md-form">
    {!! Form::label('escuela', 'Escuela:') !!}
        <label class="checkbox-inline">
{!! Form::hidden('escuela', 0) !!}
{!! Form::checkbox('escuela', '1', null) !!} 1
    </label>
</div>
-->
<!--Gerentes select-->
<select class="mdb-select md-form colorful-select dropdown-success" searchable="Buscar...">
    <option value="" selected disabled>Seleccione un gerente</option>
    @forelse($gerentes as $item)
        <option value="{{$item->id}}">{{$item->nombre}}</option>
    @empty
        <option value="">Sin Gerentes</option>
    @endforelse
</select>

<!--Marcas select-->
<select class="mdb-select md-form colorful-select dropdown-success" searchable="Buscar...">
    <option value="" selected disabled>Seleccione una Marca</option>
    @forelse($marcas as $item)
        <option value="{{$item->id}}">{{$item->marca}}</option>
    @empty
        <option value="">Sin marcas</option>
    @endforelse
</select>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('proyectos.index') !!}" class="btn btn-default">Cancelar</a>
</div>
<br>
<br>

@push('scripts')
    $(document).ready(function() {
    $('.mdb-select').materialSelect();
    });
@endpush

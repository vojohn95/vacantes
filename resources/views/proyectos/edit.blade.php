@extends('layouts.app')

@section('content')

    <div class="card card-cascade wilder">
        <!-- Card image -->
        <div class="view view-cascade gradient-card-header default-color">
            <!-- Title -->
            <h3 class="card-header-title">Estacionamiento</h3>
        </div>
    </div>
    <hr>

    <div class="row justify-content-md-center">
        <div class="col-md-6">
        @include('layouts.errors')
        {!! Form::model($proyecto, ['route' => ['proyectos.update', $proyecto->id], 'method' => 'patch','enctype' => 'multipart/form-data']) !!}
        <!-- Nombre Field -->
            <div class="md-form">
                <i class="far fa-address-card prefix"></i>
                {!! Form::label('nombre', 'Proyecto:') !!}
                {!! Form::text('nombre', null, ['class' => 'form-control', 'id' => 'nombre']) !!}
            </div>

            <div class="md-form">
                <i class="fas fa-list-ol prefix"></i>
                {!! Form::label('no_est', 'Numero de Estacionamiento:') !!}
                {!! Form::number('no_est', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Pais Field -->
            <div class="md-form">
                <i class="fas fa-city prefix"></i>
                {!! Form::label('pais', 'País:') !!}
                {!! Form::text('pais', null, ['class' => 'form-control', 'id' => 'pais']) !!}
            </div>

            <!-- Edo Prov Field -->
            <div class="md-form">
                <i class="fas fa-map-marked-alt prefix"></i>
                {!! Form::label('edo_prov', 'Estado/Provincia:') !!}
                {!! Form::text('edo_prov', null, ['class' => 'form-control', 'id' => 'estado']) !!}
            </div>

            <!-- Mun Cd Field -->
            <div class="md-form">
                <i class="fas fa-map-signs prefix"></i>
                {!! Form::label('mun_cd', 'Municipio:') !!}
                {!! Form::text('mun_cd', null, ['class' => 'form-control']) !!}
            </div>
            <!-- Colonia Field -->
            <div class="md-form">
                <i class="fas fa-map-pin prefix"></i>
                {!! Form::label('colonia', 'Colonia:') !!}
                {!! Form::text('colonia', null, ['class' => 'form-control', 'id' => 'colonia']) !!}
            </div>
            <!-- Calles Field -->
            <div class="md-form">
                <i class="fas fa-road prefix"></i>
                {!! Form::label('calles', 'Calles:') !!}
                {!! Form::text('calles', null, ['class' => 'form-control', 'id' => 'calles']) !!}
            </div>
            <!-- Numeroext Field -->
            <div class="md-form">
                <i class="fas fa-map-marker prefix"></i>
                {!! Form::label('numeroExt', 'Numeroext:') !!}
                {!! Form::text('numeroExt', null, ['class' => 'form-control', 'id' => 'numeroexterior']) !!}
            </div>
            <!-- Cp Field -->
            <div class="md-form">
                <i class="fas fa-map-marker-alt prefix"></i>
                {!! Form::label('cp', 'Codigo postal:') !!}
                {!! Form::text('cp', null, ['class' => 'form-control', 'id' => 'codigo']) !!}
            </div>
            <!-- Foto Field -->
            <div class="md-form">
                <div class="file-field">
                    <div class="btn btn-primary btn-sm float-left">
                        <span>Examinar equipo</span>
                        <input type="file" name="file" id="file"
                               accept="image/*">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text" placeholder="Cargar nueva imagen">
                    </div>
                </div>
            </div>
            <div class="md-form">
                <i class="fas fa-user prefix"></i>
                {!! Form::label('gerente', 'Gerente:') !!}
                {!! Form::text('gerente', null, ['class' => 'form-control']) !!}
            </div>
            <!--Gerentes select-->
            <select class="mdb-select md-form colorful-select dropdown-success" searchable="Buscar..." name="gerente">
                <option value="" selected disabled>Seleccione un nuevo gerente</option>
                @forelse($gerentes as $item)
                    <option value="{{$item->id}}">{{$item->nombre}}</option>
                @empty
                    <option value="">Sin Gerentes</option>
                @endforelse
            </select>
            <div class="md-form">
                <i class="fas fa-bookmark prefix"></i>
                {!! Form::label('marca', 'Marca:') !!}
                {!! Form::text('marca', null, ['class' => 'form-control']) !!}
            </div>
            <!--Marcas select-->
            <select class="mdb-select md-form colorful-select dropdown-success" searchable="Buscar..." name="marca">
                <option value="" selected disabled>Seleccione nueva Marca</option>
                @forelse($marcas as $item)
                    <option value="{{$item->id}}">{{$item->marca}}</option>
                @empty
                    <option value="">Sin marcas</option>
                @endforelse
            </select>
            <div class="md-form">
                <i class="fas fa-school prefix"></i>
                {!! Form::label('escuela', 'Escuela:') !!}
                @if($item->escuela)
                    {!! Form::text('escuela', 'Si', ['class' => 'form-control']) !!}
                @else
                    {!! Form::text('escuela', 'No', ['class' => 'form-control']) !!}
                @endif
            </div>
            <!--Blue select-->
            <select class="mdb-select md-form colorful-select dropdown-success" name="escuela">
                <option value="" selected disabled>¿Es escuela?</option>
                <option value="true">Si</option>
                <option value="false">No</option>
            </select>
            <!--/Blue select-->
            <!-- Direccion Field -->
        {!! Form::hidden('num_vac', 0 , ['name' => 'num_vac']) !!}

        {!! Form::hidden('direccion', null, ['id' => 'direccion', 'name' => 'direccion']) !!}

        {{ Form::hidden('latitud', null , ['id' => 'latitud', 'name' => 'latitud']) }}

        {{ Form::hidden('longitud', null , ['id' => 'longitud', 'name' => 'longitud']) }}


        <!-- Submit Field -->
            <div class="form-group col-sm-12">
                {!! Form::submit('Actualiza', ['class' => 'btn btn-primary', 'id' => 'guardar', 'onclick' => "return confirm('¿Has confirmado que la dirección sea correcta?')"]) !!}
                <a href="{!! route('proyectos.index') !!}" class="btn btn-default">Atras</a>
                <!--<input type="button" name="boton01" id="boton01" value="Previsualizar" class="btn btn-deep-purple">-->

            </div>
            <br>
            <br>

            {!! Form::close() !!}
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col">
                    <!-- Card -->
                    <div class="card map-card">
                        <!--Google map-->
                        <div id="map_canvas" class="z-depth-1-half map-container" style="height: 500px"></div>
                        <!-- Card content -->
                    </div>
                    <!-- Card -->
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col">
                    <!-- Card -->
                    <div class="card">
                        <!-- Card image -->
                        <img class="card-img-top" src="data:image/jpg;base64,{{$proyecto->foto}}" alt="Card image cap">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script>

    @push('scripts')
    //Variables
    var map = null;
    var infoWindow = null;
    var resultados_lat = "";
    var resultados_long = "";
    var direc = "";
    var mensajeError = "";

    $(document).ready(function () {
        $('.mdb-select').materialSelect();
        //input de nombre
        $("#nombre").keyup(function () {
            var value = $(this).val();
            $("#proyecto").text(value);
        });

        //input de direccion
        $("#calles").keyup(function () {
            var value = $(this).val();
            $("#calle").text(value);
        });

        //input de numero exterior
        $("#numeroexterior").keyup(function () {
            var value = $(this).val();
            $("#num_ext").text(value);
        });

        //input de colonia
        $("#colonia").keyup(function () {
            var value = $(this).val();
            $("#colonia_id").text(value);
        });

        //input de codigo postal
        $("#codigo").keyup(function () {
            var value = $(this).val();
            $("#codigop").text(value);
        });

        //input de pais
        $("#pais").keyup(function () {
            var value = $(this).val();
            $("#pais_id").text(value);
        });

        //input de estado
        $("#estado").keyup(function () {
            var value = $(this).val();
            $("#estado_id").text(value);
        });
        $('#map_canvas').show();
        direc = $("#direccion").val();
        initialize();

    });

    //Script para Validar datos y Mostrar mapa
    $('#boton01').click(function () {
        if ($('#nombre').val() === '' || $('#calles').val() === '' || $('#numeroexterior').val() === '' || $('#colonia').val() === '' || $('#codigo').val() === '' || $('#estado').val() === '' || $('#pais').val() === '') {
            alert('Hay campos sin completar');
        } else {
            direc = $("#calles").val() + " " + $("#numeroexterior").val() + ", " + $("#colonia").val() + ", " + $("#codigo").val() + ", " + $("#estado").val() + " " + $("#pais").val() + " ";
            $("#direccion").val(direc);
            initialize();
        }
    });

    //Codigo Para generar Mapa

    //Se hace peticion de mapa
    function initialize() {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': direc}, function (results, status) {
            if (status === 'OK') {
                var resultados = results[0].geometry.location,
                    resultados_lat = resultados.lat(),
                    resultados_long = resultados.lng();
                //alert("Latitud: " +resultados_lat + " longitud: " + resultados_long);
                $("#latitud").val(resultados_lat);
                $("#longitud").val(resultados_long);
                positionMap(resultados_lat, resultados_long);
                //Se muestra el boton de guardar
                //$('#guardar').show();
            } else {
                if (status === "ZERO_RESULTS") {
                    mensajeError = "No hubo resultados para la dirección ingresada.";
                } else if (status === "OVER_QUERY_LIMIT" || status === "REQUEST_DENIED" || status === "UNKNOWN_ERROR") {
                    mensajeError = "Error general del mapa.";
                } else if (status === "INVALID_REQUEST") {
                    mensajeError = "Error de la web. Contacte con Name Agency.";
                }
                alert(mensajeError);
            }
        });
    }

    function positionMap(latd, long) {
        var myLatlng = {lat: latd, lng: long};

        var image = {

            url: '{{asset('logo/logo/marker-central-2.png')}}',
            // This marker is 20 pixels wide by 32 pixels high.
            size: new google.maps.Size(25, 25),
            // The origin for this image is (0, 0).
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at (0, 32).
            anchor: new google.maps.Point(14, 30)
        };


        var myOptions = {
            zoom: 13,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);

        infoWindow = new google.maps.InfoWindow();

        var marker = new google.maps.Marker({
            position: myLatlng,
            draggable: true,
            icon: image,
            map: map,
            title: 'Ejemplo marcador arrastrable'
        });
        google.maps.event.addListener(marker, 'drag', function () {
            openInfoWindow2(marker);
        });
        google.maps.event.addListener(marker, 'dragend', function () {
            openInfoWindow(marker);
        });
    }

    function openInfoWindow(marker) {
        var markerLatLng = marker.getPosition();
        infoWindow.setContent([
            'La posicion del marcador es: ',
            markerLatLng.lat(),
            ', ',
            markerLatLng.lng(),
            ' Arrastrame y haz click para actualizar la posición.'
        ].join(''));
        infoWindow.open(map, marker);
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                console.log(results);
                var address = results[0]['formatted_address'];
                $("#direccion").val(address);
                var componente = results[0]['address_components'];
                console.log(componente);
                $('#colonia').val(componente[2]['long_name']);
                $('#colonia_id').text(componente[2]['long_name']);
                $('#calles').val(componente[1]['long_name']);
                $("#calle").text(componente[1]['long_name']);
                $('#numeroexterior').val(componente[0]['long_name']);
                $('#num_ext').text(componente[0]['long_name']);
                $('#codigo').val(componente[6]['long_name']);
                $('#codigop').text(componente[6]['long_name']);
            }
        });
    }

    function openInfoWindow2(marker) {
        var markerLatLng = marker.getPosition();
        $("#latitud").val(markerLatLng.lat());
        $("#longitud").val(markerLatLng.lng());
    }

    @endpush

</script>

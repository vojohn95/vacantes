<div class="table-responsive ">
    <div class="form-group pull-right">
        <input type="text" class="search form-control" placeholder="¿Que estas buscando?">
    </div>
    <table class="table table-hover table-bordered results responsive-table text-center">
        <thead>
        <tr>
            <th>Nombre</th>
            <th>Direccion</th>
            <th>Gerente</th>
            <th>Tipo</th>
            <th>Acción</th>
        </tr>
        <tr class="warning no-result">
            <td colspan="12" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i> No se encontro
                registro con la información ingresada
            </td>
        </tr>
        </thead>

        <tbody>
        @foreach($proyectos as $proyecto)
            <tr>
                <td>{!! $proyecto->nombre !!}</td>
                <td>{!! wordwrap($proyecto->direccion,60,"<br />\n") !!}</td>
                <td>{!! $proyecto->gerente !!}</td>
                <td>
                    @if($proyecto->escuela)
                        <i class="fas fa-school fa-2x material-tooltip-main" data-toggle="tooltip"
                           data-placement="bottom" title="Escuela"></i>
                    @else
                        <i class="fas fa-parking fa-2x  material-tooltip-main" data-toggle="tooltip"
                           data-placement="bottom" title="Estacionamiento"></i>
                    @endif
                </td>

                {!! Form::open(['route' => ['proyectos.destroy', $proyecto->id], 'method' => 'delete']) !!}
                <td>
                    <a href="{!! route('proyectos.edit', [$proyecto->id]) !!}" class='btn btn-default btn-xs white-text'><i
                            class="glyphicon glyphicon-edit"></i>Visualizar</a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"><span>Eliminar</span></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('¿Estas seguro?')"]) !!}

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>


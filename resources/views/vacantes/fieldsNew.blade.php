<div class="row">
    <div class="col">
        <!-- Id Proyecto Field -->
        <select class="mdb-select md-form colorful-select dropdown-success" searchable="Buscar..."
                name="estacionamiento">
            <option value="" selected disabled>Seleccione un estacionamiento</option>
            @forelse($proyectos as $item)
                <option value="{{$item->id}}">{{$item->nombre}}</option>
            @empty
                <option value="">Sin estacionamientos</option>
            @endforelse
        </select>
    </div>
    <div class="col">
        <!-- Id Puesto Field -->
        <select class="mdb-select md-form colorful-select dropdown-success" searchable="Buscar..." name="puesto">
            <option value="" selected disabled>Seleccione un puesto</option>
            @forelse($puestos as $item)
                <option value="{{$item->id}}">{{$item->puesto}}</option>
            @empty
                <option value="">Sin puestos</option>
            @endforelse
        </select>
    </div>
</div>

<!-- Id User Field -->
{!! Form::hidden('id_user', Auth::user()->id, ['name' => 'user']) !!}

<!-- Horario Field -->
<div class="row">
    <div class="col">
        <div class="md-form">
            <i class="fas fa-business-time prefix"></i>
            <input placeholder="Selecciona el horario de entrada" type="text" id="input_starttime" name="inicio"
                   class="form-control timepicker">
            <label for="input_starttime">Inicia</label>
        </div>
    </div>
    <div class="col">
        <div class="md-form">
            <i class="fas fa-business-time prefix"></i>
            <input placeholder="Selecciona el horario de salida" type="text" id="input_starttime2"
                   name="fin" class="form-control timepicker">
            <label for="input_starttime2">Fin</label>
        </div>
    </div>
</div>

<!-- Fecha Field -->
<div class="md-form">
    <i class="fas fa-calendar prefix"></i>
    <input placeholder="Seleccione una fecha" type="text" id="fecha" class="form-control datepicker" name="fecha">
    <label for="fecha">Fecha</label>
</div>

<!-- Salario Field -->
<div class="md-form">
    <i class="fas fa-money-bill-wave prefix"></i>
    {!! Form::label('salario', 'Salario:') !!}
    {!! Form::number('salario', null, ['class' => 'form-control']) !!}
</div>


<!-- Tel Field -->
<div class="md-form">
    <i class="fas fa-mobile prefix"></i>
    {!! Form::label('tel', 'Tel:') !!}
    {!! Form::text('tel', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="md-form">
    <i class="far fa-envelope prefix"></i>
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Estado Field -->
{!! Form::hidden('estado', 'Activa',['name' => 'estado']) !!}


<!-- Submit Field -->
<div class="md-form">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary', 'id'=>'guardar']) !!}
    <a href="{!! route('vacantes.index') !!}" class="btn btn-default">Cancelar</a>
</div>

<script>
    @push('scripts')

    var horario = "";
    $(document).ready(function () {
        $('.mdb-select').materialSelect();
    });

    $('#input_starttime').pickatime({
        twelvehour: true,
        timeFormat: 'hh:mm:ss.000', // format of the time value (data-time attribute)
        format: 'h:mm tt',    // format of the input value
        theme: 'blue',        // theme of the timepicker
        readOnly: true,       // determines if input is readonly
        hourPadding: false    // determines if display value has zero padding for hour value less than 10 (i.e. 05:30 PM); 24-hour format has padding by default
    });

    $('#input_starttime2').pickatime({
        twelvehour: true,
        timeFormat: 'hh:mm:ss.000', // format of the time value (data-time attribute)
        format: 'h:mm tt',    // format of the input value
        theme: 'blue',        // theme of the timepicker
        readOnly: true,       // determines if input is readonly
        hourPadding: false
    });


    $('.datepicker').pickadate({
        monthsFull: ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'],
        monthsShort: ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'],
        weekdaysFull: ['domingo', 'lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábado'],
        weekdaysShort: ['dom', 'lun', 'mar', 'mié', 'jue', 'vie', 'sáb'],
        today: '',
        clear: 'Borrar',
        close: 'Cerrar',
        firstDay: 1,
        format: 'yyyy-mm-dd',
        formatSubmit: 'yyyy-mm-dd'
    });

    @endpush
</script>

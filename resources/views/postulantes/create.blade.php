@extends('layouts.app')
@section('title','Postularse')
@section('content')
    <div class="card card-cascade wilder">
        <!-- Card image -->
        <div class="view view-cascade gradient-card-header default-color">
            <!-- Title -->
            <h3 class="card-header-title">Solicitud</h3>
        </div>
    </div>
    <hr>

    @include('layouts.errors')

    {!! Form::open(['route' => 'postulantes.store']) !!}
    <div class="row justify-content-md-center">
        <div class="col-md-8">
            @include('postulantes.fields')
        </div>
    </div>

    {!! Form::close() !!}

    <br>
@endsection


<div class="table-responsive">
    <table class="table" id="postulantes-table">
        <thead>
        <tr>
            <th>Id Vacante</th>
            <th>Nombre</th>
            <th>Correo</th>
            <th>Tel</th>
            <th>Direccion</th>
            <th>Pais</th>
            <th>Edo Prov</th>
            <th>Mun Cd</th>
            <th>Colonia</th>
            <th>Calles</th>
            <th>Numeroext</th>
            <th>Cp</th>
            <th>Estado</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($postulantes as $postulante)
            <tr>
                <td>{!! $postulante->id_vacante !!}</td>
                <td>{!! $postulante->nombre !!}</td>
                <td>{!! $postulante->correo !!}</td>
                <td>{!! $postulante->tel !!}</td>
                <td>{!! $postulante->direccion !!}</td>
                <td>{!! $postulante->pais !!}</td>
                <td>{!! $postulante->edo_prov !!}</td>
                <td>{!! $postulante->mun_cd !!}</td>
                <td>{!! $postulante->colonia !!}</td>
                <td>{!! $postulante->calles !!}</td>
                <td>{!! $postulante->numeroExt !!}</td>
                <td>{!! $postulante->cp !!}</td>
                <td>{!! $postulante->estado !!}</td>
                <td>
                    {!! Form::open(['route' => ['postulantes.destroy', $postulante->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('postulantes.show', [$postulante->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('postulantes.edit', [$postulante->id]) !!}" class='btn btn-default btn-xs'><i
                                class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
